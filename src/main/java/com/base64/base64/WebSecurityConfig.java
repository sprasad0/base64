package com.base64.base64;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;


@Configuration
@EnableWebSecurity(debug=true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter{
	
	@Override
    protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable();	
	
	http.authorizeRequests().antMatchers("/base64").permitAll();
	

	}
	
	@Override
	  public void configure(WebSecurity web) throws Exception {
	    web
	      .ignoring().antMatchers("/css/**", "/js/**", "/images/**","/index.html");
	      
	  }

}
